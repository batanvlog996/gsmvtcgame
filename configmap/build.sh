kubectl delete configmap config-all
kubectl create configmap config-all --from-file=./configall
# create nginx 
kubectl delete configmap nginx-conf
kubectl create configmap nginx-conf --from-file=./nginx-conf
# create 
kubectl delete configmap private-key3
kubectl create configmap private-key3 --from-file=./private_key3.pem
kubectl delete configmap private-key2
kubectl create configmap private-key2 --from-file=./private_key2.pem
kubectl delete configmap private-key
kubectl create configmap private-key --from-file=./private_key.pem
kubectl delete configmap certificate3
kubectl create configmap certificate3 --from-file=./certificate3.pem
kubectl delete configmap certificate2
kubectl create configmap certificate2 --from-file=./certificate2.pem
kubectl delete configmap certificate
kubectl create configmap certificate --from-file=./certificate.pem
